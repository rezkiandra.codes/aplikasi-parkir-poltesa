function validate() {
    var fname = document.getElementById("fname");
    var lname = document.getElementById("lname");
    var age = document.getElementById("age");
    var mob = document.getElementById("mob");
    var EmailId = document.getElementById("email");
    var pw = document.getElementById("pw");
    var cpw = document.getElementById("cpw");
    var alphaExp = /^[a-zA-Z]+$/;
    var atpos = EmailId.value.indexOf("@");
    var dotpos = EmailId.value.lastIndexOf(".");
    if (fname.value == null || fname.value == "") {
        fname.focus();
        alert("Masukkan nama depan yang valid");
        return false;
    }
    if (fname.value.match(alphaExp)) {
    } else {
        alert("Nama depan hanya bisa berupa huruf");
        fname.focus();
        return false;
    }
    if (lname.value == null || lname.value == "") {
        lname.focus();
        alert("Masukkan nama belakang yang valid");
        return false;
    }
    if (lname.value.match(alphaExp)) {
    } else {
        alert("Nama belakang hanya bisa berupa huruf");
        lname.focus();
        return false;
    }
    if (age.value == null || age.value == "") {
        alert("Masukkan umur");
        age.focus();
        return false;
    }
    if (isNaN(age.value)) {
        alert("Umur harus berupa angka");
        age.focus();
        return false;
    }
    if (mob.value == null || mob.value == " ") {
        alert("Masukkan NO. HP");
        mob.focus();
        return false;
    }
    if (isNaN(mob.value)) {
        alert("NO. HP harus berupa angka");
        mob.focus();
        return false;
    }
    if (atpos < 1 || dotpos < atpos + 2 || dotpos + 2 >= EmailId.value.length) {
        alert("Masukkan Email yang valid");
        EmailId.focus();
        return false;
    }
    if (pw.value.length < 8 || cpw.value.length < 8) {
        alert("Password minimal 8 karakter");
        pw.focus();
        return false;
    } else if (pw.value.length != cpw.value.length) {
        alert("Password tidak valid");
        pw.focus();
        return false;
    } else if (pw.value != cpw.value) {
        alert("Password tidak valid");
        pw.focus();
        return false;
    }
    return true;
}
