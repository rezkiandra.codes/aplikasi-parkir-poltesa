<HTML>

<HEAD>
    <TITLE>Aplikasi Parkir Poltesa</TITLE>
    <style type="text/css">
        @import url(style.css);

        * {
            color: white;
        }

        #main {
            width: 700px;
            height: 400px;
            margin: 0 auto;
            margin-top: 120px;
            color: white;
            border-radius: 25px;
            padding-top: 10px;
            padding-right: 10px;
            padding-bottom: 10px;
            padding-left: 10px;
            background-color: rgba(0, 0, 0, 0.5);
        }
    </style>
</HEAD>

<BODY>
    <?php
    session_start();
    include("header2.php"); ?>
    <div id="main">
        <div id="logo">
            <A HREF="index.php">
                <IMG SRC="img/white.png" alt="Home" id="logo" width="150" height="150"></IMG>
            </A>
        </div>
        <h1 class="text-center mb-3 text-uppercase">Selamat Datang di<br>Aplikasi Parkir Poltesa</h1>
        <h5 class="text-center mb-5">Silahkan Pilih Tempat Parkir</h5>
        <?php
        if (isset($_SESSION['user_info'])) {
            echo '<h3 align="center" class="text-uppercase text-decoration-none"><a class="col-3 btn btn-sm btn-danger" href="logout.php">Logout</a></h3>';
        } else {
            echo "<script type='text/javascript'>alert('Please login before proceeding further!');</script>";
            echo '<script>location.href = "login.php";</script>';
        }
        ?>
    </div>
</BODY>

</HTML>