<!DOCTYPE html>
<html>

<head>
    <title>Aplikasi Parkir Poltesa</title>
    <link rel="stylesheet" href="s1.css" type="text/css">
    <style type="text/css">
        li {
            font-family: sans-serif;
            font-size: 18px;
        }

        body {
            background: url(img/home4.jpg);
            background-position: center;
            background-repeat: no-repeat;
            background-size: cover;
            height: 100vh;
        }
    </style>
    <script src="jquery.js"></script>
    <!-- <script>
        $(document).ready(function() {
                    $("#Logout").hide();
                }; $(document).ready(function() {
                    $("#user").hover(function() {
                        $("#Logout").toggle("slow");
                    });
                });
    </script> -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
</head>

<body>
    <div class="navbar bg-primary d-flex justify-content-evenly">
        <a href="uhome.php" class="text-light fw-bold text-decoration-none text-uppercase">Beranda</a>
        <a href="booktkt.php" class="text-light fw-bold text-decoration-none text-uppercase">Booking</a>
        <a href="pnrstatus.php" class="text-light fw-bold text-decoration-none text-uppercase">Detail</a>
        <a href="unbook.php" class="text-light fw-bold text-decoration-none text-uppercase">Unbooking</a>
        <a href="logout.php" class="text-light fw-bold text-decoration-none text-uppercase">Logout</a>
    </div>

</html>