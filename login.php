<?php
session_start();
if (isset($_POST['submit'])) {
    $conn = mysqli_connect("localhost", "root", "", "Customer");
    if (!$conn) {
        echo "<script type='text/javascript'>alert('Database failed');</script>";
        die('Could not connect: ' . mysqli_connect_error());
    }
    $email = $_POST['email'];
    $pw = $_POST['pw'];
    $sql = "SELECT * FROM Customer WHERE email = '$email' AND password = '$pw'";
    $sql_result = mysqli_query($conn, $sql) or die('request "Could not execute SQL query" ' . $sql);
    $user = mysqli_fetch_assoc($sql_result);
    if (!empty($user)) {
        $_SESSION['user_info'] = $user['email'];
        $message = 'Logged in successfully';
        header("Location: uhome.php");
    } else {
        $message = 'Wrong email or password.';
    }
    echo "<script type='text/javascript'>alert('$message');</script>";
}
?>
<!DOCTYPE html>
<html>

<head>
    <title>Login</title>
</head>
<script type="text/javascript">
    function validate() {
        var EmailId = document.getElementById("email");
        var atpos = EmailId.value.indexOf("@");
        var dotpos = EmailId.value.lastIndexOf(".");
        var pw = document.getElementById("pw");
        if (atpos < 1 || dotpos < atpos + 2 || dotpos + 2 >= EmailId.value.length) {
            alert("Enter valid email-ID");
            EmailId.focus();
            return false;
        }
        if (pw.value.length < 8) {
            alert("Password consists of atleast 8 characters");
            pw.focus();
            return false;
        }
        return true;
    }
</script>
<style type="text/css">
    #loginarea {
        width: 450px;
        height: 300px;
        margin-top: 170px;
        margin-left: 470px;
        color: white;
        border-radius: 25px;
        padding-top: 10px;
        padding-right: 10px;
        padding-bottom: 10px;
        padding-left: 10px;
        background-color: rgb(0, 0, 0, 0.6);
        position: relative;
        z-index: 5;
    }

    body {
        background: url(img/home3.jpg);
        background-repeat: no-repeat;
        background-position: center;
        background-size: cover;
        height: 100vh;
    }

    #logintext {
        text-align: center;
    }

    .data {
        color: white;
    }
</style>

<body>
    <?php include("header.php") ?>
    <div id="loginarea">
        <form id="login" action="login.php" onsubmit="return validate()" method="post" name="login">
            <div id="logintext">
                <h4 class="text-uppercase my-4">Login User Pengguna</h4>
                <hr>
            </div>
            <div class="form">
                <div class="d-flex flex-row justify-content-center align-items-center">
                    <div class="col-4">
                        <label class="text-uppercase" for="email">Email :</label>
                    </div>
                    <div class="col-8">
                        <input class="my-3" type="text" id="email" size="30" maxlength="30" name="email" placeholder="Masukkan Email" />
                    </div>
                </div>
                <div class="d-flex flex-row justify-content-center align-items-center">
                    <div class="col-4">
                        <label class="text-uppercase" for="pw">Password :</label>
                    </div>
                    <div class="col-8">
                        <input class="my-2" type="password" id="pw" size="30" maxlength="30" name="pw" placeholder="Masukkan Password" />
                    </div>
                </div>
            </div>
            <div class="d-flex justify-content-center">
                <INPUT class="btn text-uppercase btn-sm mt-4 offset-3 col-7 btn-success" TYPE="Submit" value="Submit" name="submit" id="submit">
            </div>
        </form>
    </div>
</body>

</html>