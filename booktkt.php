<?php
session_start();
if (empty($_SESSION['user_info'])) {
    echo "<script type='text/javascript'>alert('Login terlebih dahulu sebelum melakukan proses');</script>";
}
if (isset($_POST['submit'])) {
    if (isset($_POST['Location'])) {
        $trains = $_POST['Location'];
        if ($trains == 'Gedung Kuliah Terpadu I') {
            echo '<script>location.href = "slot.php";</script>';
        }
        if ($trains == 'Gedung Kuliah Terpadu II') {
            echo '<script>location.href = "slotmensh.php";</script>';
        }
        if ($trains == 'Gedung Lab Terpadu') {
            echo '<script>location.href = "slotvitm.php";</script>';
        }
    }
}
?>
<!DOCTYPE html>
<html>

<head>
    <title>Lokasi Parkir</title>
    <link rel="stylesheet" href="style.css">
    <style type="text/css">
        #booktkt {
            margin: auto;
            margin-top: 170px;
            width: 30%;
            height: 41%;
            padding: auto;
            padding-top: 50px;
            background-color: rgba(0, 0, 0, 0.5);
            border-radius: 25px;
        }

        #trains {
            /* margin-left: 90px; */
            font-size: 15px;
        }
    </style>
</head>

<body>
    <?php
    include('header2.php');
    ?>
    <div id="booktkt">
        <h3 class="text-center text-uppercase text-light" id="journeytext">Lokasi Parkir</h3>
        <hr class="bg-light mb-5">
        <form method="post" name="journeyform">
            <div class="d-flex justify-content-center">
                <select class="px-4 py-1" id="Location" name="Location" required>
                    <option selected disabled>- Pilih Gedung</option>
                    <option value="Gedung Kuliah Terpadu I">Gedung Kuliah Terpadu I</option>
                    <option value="Gedung Kuliah Terpadu II">Gedung Kuliah Terpadu II</option>
                    <option value="Gedung Lab Terpadu">Gedung Lab Terpadu</option>
                </select>
            </div>
            <div class="d-flex justify-content-center">
                <input class="btn btn-sm col-7 btn-success text-uppercase mt-4" type="submit" name="submit" id="submit" class="button" />
            </div>
        </form>
    </div>
</body>

</html>