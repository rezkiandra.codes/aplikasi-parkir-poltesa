<?php
session_start();

if (isset($_POST['submit'])) {
    $trains = $_POST['Location'];
    if ($trains == 'Gedung Kuliah Terpadu I') {
        echo '<script>location.href = "removeslotsjt.php";</script>';
    }
    if ($trains == 'Gedung Kuliah Terpadu II') {
        echo '<script>location.href = "removeslotmensh.php";</script>';
    }
    if ($trains == 'Gedung Lab Terpadu') {
        echo '<script>location.href = "removeslotvitm.php";</script>';
    }
}
?>
<!DOCTYPE html>
<html>

<head>
    <title>Hapus Parkiran</title>
</head>
<style type="text/css">
    #loginarea {
        background-color: white;
        width: 30%;
        margin: auto;
        border-radius: 25px;
        margin-top: 180px;
        background-color: rgba(0, 0, 0, 0.6);
        padding: 40px;
        font-family: sans-serif;
        color: white;
        box-shadow: 5px 5px 5px black;
    }

    body {
        background: url(../img/manager4.jpg);
        background-position: center;
        background-repeat: no-repeat;
        background-size: cover;
        height: 100vh;
    }

    #logintext {
        text-align: center;
    }

    .data {
        color: white;
    }
</style>

<body>
    <?php include("mheader.php") ?>
    <div id="loginarea">
        <form id="login" action="removeslot.php" method="post" name="login">
            <div id="logintext" class="text-uppercase">Hapus Spot Parkiran</div>
            <hr class="bg-light">
            <div class="d-flex flex-column justify-content-center align-items-center">
                <select class="px-2 mt-3" id="Location" name="Location" required>
                    <option selected disabled>- Pilih Lokasi Gedung</option>
                    <option value="Gedung Kuliah Terpadu I">Gedung Kuliah Terpadu I</option>
                    <option value="Gedung Kuliah Terpadu II">Gedung Kuliah Terpadu II</option>
                    <option value="Gedung Lab Terpadu">Gedung Lab Terpadu</option>
                </select>
                <INPUT class="mt-4 col-4 btn btn-sm text-uppercase btn-danger" TYPE="Submit" value="Submit" name="submit" id="submit" class="button">
            </div>
        </form>
    </div>
</body>

</html>