<?php
session_start();
if (isset($_POST['submit'])) {
    $conn = mysqli_connect("localhost", "root", "", "Customer");
    if (!$conn) {
        echo "<script type='text/javascript'>alert('Database failed');</script>";
        die('Could not connect: ' . mysqli_connect_error());
    }
    $email = $_POST['email'];
    $pw = $_POST['pw'];
    $sql = "SELECT * FROM manager WHERE email = '$email' AND password = '$pw'";
    $sql_result = mysqli_query($conn, $sql) or die('request "Could not execute SQL query" ' . $sql);
    $user = mysqli_fetch_assoc($sql_result);
    if (!empty($user)) {
        $_SESSION['user_info'] = $user['email'];
        $message = 'Logged in successfully';
        header("Location: home.php");
    } else {
        $message = 'Wrong email or password.';
    }
    echo "<script type='text/javascript'>alert('$message');</script>";
}
?>
<!DOCTYPE html>
<html>

<head>
    <title>Login</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
</head>
<script type="text/javascript">
    function validate() {
        var EmailId = document.getElementById("email");
        var atpos = EmailId.value.indexOf("@");
        var dotpos = EmailId.value.lastIndexOf(".");
        var pw = document.getElementById("pw");
        if (atpos < 1 || dotpos < atpos + 2 || dotpos + 2 >= EmailId.value.length) {
            alert("Enter valid email-ID");
            EmailId.focus();
            return false;
        }
        if (pw.value.length < 8) {
            alert("Password consists of atleast 8 characters");
            pw.focus();
            return false;
        }
        return true;
    }
</script>
<style type="text/css">
    #loginarea {
        background-color: white;
        width: 30%;
        margin: auto;
        border-radius: 25px;
        margin-top: 180px;
        background-color: rgba(0, 0, 0, 0.6);
        padding: 40px;
        color: white;
        font-size: 17px;
        box-shadow: 2px 2px 5px red;
    }

    body {
        background-image: url(../img/manager1.jpg);
        background-position: center;
        background-repeat: no-repeat;
        background-size: cover;
        height: 100vh;
    }

    #logintext {
        text-align: center;
    }

    .data {
        color: white;
    }
</style>


<body>
    <?php include("go_back_mlogin.php") ?>
    <div id="loginarea">
        <form id="login" action="manager.php" onsubmit="return validate()" method="post" name="login">
            <div id="logintext" class=text-uppercase>Admin Login</div>
            <hr class="bg-light">
            <div class="d-flex justify-content-center align-items-center mt-2">
                <div class="col-4">
                    <label for="email">Email :</label>
                </div>
                <div class="col-8">
                    <input type="text" id="email" maxlength="30" name="email" placeholder="Masukkan Email"/>
                </div>
            </div>
            <div class="d-flex justify-content-center align-items-center mt-4">
                <div class="col-4">
                    <label for="pw">Password :</label>
                </div>
                <div class="col-8">
                    <input type="password" id="pw" maxlength="30" name="pw" placeholder="Masukkan Password"/>
                </div>
            </div>
            <div class="d-flex justify-content-center align-items-center">
                <INPUT class="offset-3 col-6 btn btn-sm mt-4 btn-danger text-uppercase" TYPE="Submit" value="Submit" name="submit" id="submit" class="button">
            </div>
        </form>
    </div>
</body>

</html>