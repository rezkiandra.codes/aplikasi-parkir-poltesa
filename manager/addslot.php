<?php
session_start();
$conn = mysqli_connect("localhost", "root", "", "Customer");
if (!$conn) {
    echo "<script type='text/javascript'>alert('Database failed');</script>";
    die('Could not connect: ' . mysqli_connect_error());
}
if (isset($_POST['submit'])) {
    $email = $_POST['emal'];
    $trains = $_POST['Location'];
    if ($trains == 'Gedung Kuliah Terpadu I') {
        $sql = "INSERT INTO SJT (status, spot_id) VALUES ('NBooked',$email)";
        $sql_result = mysqli_query($conn, $sql) or die('request "Could not be executed" ' . $sql);
        $sql = "SELECT * FROM SJT";
        $sql_result = mysqli_query($conn, $sql) or die('request "Could not be executed" ' . $sql);
        $user = mysqli_fetch_assoc($sql_result);
        if (!empty($user)) {
            $message = 'ADDED successfully';
        } else {
            $message = 'Enter Correct details';
        }
        echo "<script type='text/javascript'>alert('$message');</script>";
    }
    if ($trains == 'Gedung Kuliah Terpadu II') {
        $sql = "INSERT INTO mensh (status, spot_id) VALUES ('NBooked',$email)";
        $sql_result = mysqli_query($conn, $sql) or die('request "Could not be executed" ' . $sql);
        $sql = "SELECT * FROM mensh";
        $sql_result = mysqli_query($conn, $sql) or die('request "Could not be executed" ' . $sql);
        $user = mysqli_fetch_assoc($sql_result);
        if (!empty($user)) {
            $message = 'ADDED successfully';
        } else {
            $message = 'Enter Correct details';
        }
        echo "<script type='text/javascript'>alert('$message');</script>";
    }
    if ($trains == 'Gedung Lab Terpadu') {
        $sql = "INSERT INTO vit_m (status, spot_id) VALUES ('NBooked',$email)";
        $sql_result = mysqli_query($conn, $sql) or die('request "Could not be executed" ' . $sql);
        $sql = "SELECT * FROM vit_m";
        $sql_result = mysqli_query($conn, $sql) or die('request "Could not be executed" ' . $sql);
        $user = mysqli_fetch_assoc($sql_result);
        if (!empty($user)) {
            $message = 'ADDED successfully';
        } else {
            $message = 'Enter Correct details';
        }
        echo "<script type='text/javascript'>alert('$message');</script>";
    }
}
?>
<!DOCTYPE html>
<html>

<head>
    <title>Tambah Parkiran</title>
</head>
<style type="text/css">
    #loginarea {
        background-color: white;
        width: 30%;
        margin: auto;
        border-radius: 25px;
        margin-top: 180px;
        background-color: rgba(0, 0, 0, 0.6);
        padding: 40px;
        font-family: sans-serif;
        color: white;
        box-shadow: 2px 2px 5px whitesmoke;
    }

    body {
        background-image: url(../img/manager3.jpg);
        background-position: center;
        background-repeat: no-repeat;
        background-size: cover;
        height: 100vh;
    }

    #logintext {
        text-align: center;
    }

    .data {
        color: white;
    }
</style>

<body>
    <?php include("mheader.php") ?>
    <div id="loginarea">
        <form id="login" action="addslot.php" method="post" name="login">
            <div id="logintext" class="text-uppercase">Tambah Spot Parkir</div>
            <hr class="bg-light">
            <div class="d-flex flex-row justify-content-center align-items-center mb-4">
                <div class="col">
                    <label for="emal">ID Spot</label>
                </div>
                <div class="col-8">
                    <input type="number" id="emal" name="emal" min="1" maxlength="3" placeholder="Masukkan Spot Parkir"/>
                </div>
            </div>
            <div class="d-flex flex-column justify-content-center align-items-center">
                <select class="px-2" id="Location" name="Location" required>
                    <option selected disabled>- Pilih Lokasi Gedung</option>
                    <option value="Gedung Kuliah Terpadu I">Gedung Kuliah Terpadu I</option>
                    <option value="Gedung Kuliah Terpadu II">Gedung Kuliah Terpadu II</option>
                    <option value="Gedung Lab Terpadu">Gedung Lab Terpadu</option>
                </select>
                <INPUT class="mt-4 btn btn-sm col-4 btn-danger text-uppercase" TYPE="Submit" value="Submit" name="submit" id="submit" class="button">
            </div>
    </div>
    </form>
    </div>
</body>

</html>