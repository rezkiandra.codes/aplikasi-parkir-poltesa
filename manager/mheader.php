<!DOCTYPE html>
<html>

<head>
    <title></title>
    <link rel="stylesheet" href="s1.css" type="text/css">
    <style type="text/css">
        li {
            font-family: sans-serif;
        }

        html {
            background: url(../img/backgroundimage.jpg) no-repeat center center fixed;
            -webkit-background-size: cover;
            -moz-background-size: cover;
            -o-background-size: cover;
            background-size: cover;
        }
    </style>
    <script src="jquery.js"></script>
    <!-- <script>
        $(document).ready(function() {
                    $("#Logout").hide();
                }; $(document).ready(function() {
                    $("#user").hover(function() {
                        $("#Logout").toggle("slow");
                    });
                });
    </script> -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
</head>

<body>
    <div class="container-fluid bg-secondary text-uppercase ps-5">
        <ul>
            <li><A HREF="home.php">Beranda</A></li>
            <li>
                <A HREF="addslot.php">Tambah</A>
            </li>
            <li>
                <A HREF="removeslot.php">Hapus</A>
            </li>
            <li>
                <a href="blockslot.php">Blokir</a>
            </li>
            <li class="text-light">
                <?php
                if (isset($_SESSION['user_info'])) {
                    echo '<div id="dropdown">' . $_SESSION['user_info'] . '<div id="Logout" style="display:none">Logout</div>';
                }
                ?>
            </li>
        </ul>
    </div>
    </div>
</body>

</html>