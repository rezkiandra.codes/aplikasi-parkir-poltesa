<HTML>

<HEAD>
    <TITLE>Halaman Manager</TITLE>
    <style type="text/css">
        @import url(style.css);

        #logo {
            border-radius: 25px;
        }

        * {
            color: white;
        }

        body {
            background: url(../img/manager2.jpg);
            background-position: center;
            background-repeat: no-repeat;
            background-size: cover;
            height: 100vh;
        }

        #main {
            width: 600px;
            height: 400px;
            margin: 0 auto;
            margin-top: 120px;
            color: white;
            border-radius: 25px;
            padding-top: 10px;
            padding-right: 10px;
            padding-bottom: 10px;
            padding-left: 10px;
            background-color: rgba(0, 0, 0, 0.6);
            box-shadow: 5px 5px 5px black;
        }
    </style>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
</HEAD>

<BODY>
    <?php
    session_start();
    include("mheader.php"); ?>
    <div id="main">
        <div id="logo">
            <A HREF="index.php">
                <IMG SRC="../img/white.png" alt="Home" id="logo" width="150"></IMG>
            </A>
        </div>
        <h1 class="text-center mb-4 text-uppercase">Selamat Datang di<br>Halaman Admin</h1>
        <h5 class="text-center mb-4">Kelola Tempat Parkir</h5>
        <?php
        if (isset($_SESSION['user_info'])) {
            echo '<h3 align="center"><a class="btn btn-sm col-3 btn-danger text-uppercase fw-bold" href="logout.php">Logout</a></h3>';
        } else {
            echo "<script type='text/javascript'>alert('Please login before proceeding further!');</script>";
            echo '<script>location.href = "manager.php";</script>';
        }
        ?>
    </div>
</BODY>

</HTML>