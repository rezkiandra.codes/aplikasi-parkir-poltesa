<?php
session_start();
$conn = mysqli_connect("localhost", "root", "", "Customer");
if (!$conn) {
    echo "<script type='text/javascript'>alert('Database failed');</script>";
    die('Could not connect: ' . mysqli_connect_error());
}
if (isset($_POST['submit'])) {
    $fname = $_POST['fname'];
    $lname = $_POST['lname'];
    $age = $_POST['age'];
    $mob = $_POST['mob'];
    $gender = $_POST['gender'];
    $email = $_POST['email'];
    $pw = $_POST['pw'];
    $cpw = $_POST['cpw'];
    $sql = "INSERT INTO Customer (p_fname, p_lname, p_age, p_contact, p_gender, email, password) VALUES ('$fname', '$lname', '$age', '$mob', '$gender', '$email', '$pw');";
    if (mysqli_query($conn, $sql)) {
        $message = "You have been successfully registered";
    } else {
        $message = "Could not insert record";
    }
    echo "<script type='text/javascript'>alert('$message');</script>";
}
?>
<HTML>

<HEAD>
    <TITLE>Register Akun</TITLE>
    <link rel="stylesheet" Href="style.css">
    <style type="text/css">
        * {
            color: #222;
        }

        body {
            height: 100vh;
            background-image: url(./img/home2.jpg);
            background-repeat: no-repeat;
            background-size: cover;
            background-position: center;
        }

        #register_form {
            background-color: rgb(250, 250, 250, 0.9);
            width: 40%;
            margin: auto;
            border-radius: 25px;
            border: 1px solid white;
            margin-top: 40px;
            box-shadow: 1px 1px 5px #d0d0d0;
        }

        #nav {
            color: white;
        }

        #logintext {
            margin-top: 10px;
        }

        #login {
            margin-top: 10px;
            margin-bottom: 20px;
        }
    </style>
</HEAD>

<BODY link="white" alink="white" vlink="white" width="1024" height="768">
    <?php include("header.php") ?>
    <div id="register_form" align="center" height="200" width="200">
        <h3 class="text-uppercase my-4">Daftar Akun Pengguna</h2><hr>
        <FORM name="register" method="post" action="register.php" onsubmit="return validate()">
            <TABLE border="0">
                <tr></tr>
                <tr></tr>
                <tr></tr>
                <tr></tr>
                <tr></tr>
                <TR class="left">
                    <TD>
                        <FONT class="mb-3" size="3" color="WHITE">Nama Depan :</FONT>
                    </TD>
                    <TD><INPUT class="my-1" name="fname" type="TEXT" placeholder="Masukkan Nama Depan" size="30" maxlength="30" align="center" id="fname"></TD>
                </TR>
                <tr></tr>
                <tr></tr>
                <TR class="left">
                    <TD>
                        <FONT class="mb-3" size="3" color="WHITE">Nama Belakang :</FONT>
                    </TD>
                    <TD><INPUT class="my-1" type="TEXT" name="lname" align="center" size="30" maxlength="30" placeholder="Masukkan Nama Belakang" id="lname"></TD>
                </TR>
                <tr></tr>
                <tr></tr>
                <TR class="left">
                    <TD>
                        <FONT class="mb-3" size="3" color="WHITE">Umur :</FONT>
                    </TD>
                    <TD><INPUT class="my-1" type="TEXT" name="age" align="center" size="30" maxlength="3" placeholder="Masukkan Umur" id="age"></TD>
                </TR>
                <tr></tr>
                <tr></tr>
                <TR class="left">
                    <TD>
                        <FONT class="mb-3" size="3" color="WHITE">No. HP :</FONT>
                    </TD>
                    <TD><INPUT class="my-1" type="TEXT" name="mob" size="30" maxlength="13" placeholder="Masukkan No. HP" id="mob"></TD>
                </TR>
                <tr></tr>
                <tr></tr>
                <TR class="left">
                <TR class="left">
                    <TD>
                        <FONT class="mb-3" size="3" color="WHITE">Jenis Kelamin :</FONT>
                    </TD>
                    <TD>&nbsp;&nbsp;
                        <INPUT class="my-1" type="radio" name="gender" value="Male" align="center" id="gender">Laki-laki
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <input class="my-1" type="radio" name="gender" value="Female" align="center" id="gender">Perempuan
                    </TD>
                </TR>
                <tr></tr>
                <tr></tr>
                <TR class="left">
                    <TD>
                        <FONT class="mb-3" size="3" color="WHITE">Email :</FONT>
                    </TD>
                    <TD><INPUT class="my-1" name="email" type="TEXT" id="email" placeholder="Masukkan Email" size="30" maxlength="30"></TD>
                </TR>
                <tr></tr>
                <tr></tr>
                <TR class="left">
                    <TD>
                        <FONT class="mb-3" size="3" color="WHITE">Password :</FONT>
                    </TD>
                    <TD><input class="my-1" type="Password" id="pw" name="pw" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}" title="Harus berisikan huruf kapital, angka, dan minimal 8 huruf" required placeholder="Masukkan Password"></TD>
                </TR>
                <tr></tr>
                <tr></tr>
                <TR class="left">
                    <TD>
                        <FONT class="mb-3" size="3" color="WHITE">Konfirmasi Password :</FONT>
                    </TD>
                    <TD><INPUT class="my-1" type="PASSWORD" name="cpw" size="30" id="cpw" placeholder="Masukkan Konfirmasi Password"></TD>
                </TR>
                <tr></tr>
                <tr></tr>
                <tr></tr>
                <tr></tr>
                <tr></tr>
                <tr></tr>
                <tr></tr>
            </TABLE>
            <div class="d-flex flex-column justify-content-center">
                <P><INPUT class="btn mt-3 offset-4 col-6 btn-success" TYPE="Submit" value="Daftar" name="submit" id="submit" class="button" onclick="if(!this.form.tc.checked){alert('You must agree to the terms first.');return false}">&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                </P>
            </div>
        </FORM>
        <HR width="450" style="border-color: blue;display: block;" noshade>
        <FORM action="login.php">
            <P align="CENTER" id="logintext">
                <FONT size="4" color="white" face="Arial">
                    Sudah punya akun?<BR /></FONT>
                </FONT>
                <INPUT class="btn btn-warning col-4" TYPE="submit" value="Login" id="login" class="button">
            </P>
        </FORM>
    </div>
</BODY>

</HTML>