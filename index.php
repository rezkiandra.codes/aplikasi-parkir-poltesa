<HTML>

<HEAD>
    <TITLE>Aplikasi Parkir Poltesa</TITLE>
    <style type="text/css">
        @import url(style.css);

        #main {
            width: 700px;
            height: 400px;
            margin-top: 120px;
            color: white;
            border-radius: 25px;
            padding-top: 10px;
            padding-right: 10px;
            padding-bottom: 10px;
            padding-left: 10px;
            background-color: rgb(0, 0, 0, 0.6);
            position: absolute;
            z-index: 5;
            top: 20;
            left: 350;
        }

        body {
            /* background: linear-gradient(139deg, rgba(81, 90, 98, 1) 0%, rgba(108, 164, 154, 1) 50%, rgba(69, 69, 70, 1) 100%); */
            /* background: linear-gradient(139deg, rgba(108, 111, 115, 1) 0%, rgba(86, 87, 138, 1) 76%, rgba(83, 81, 115, 1) 100%); */
            /* background: linear-gradient(139deg, rgba(68, 113, 91, 1) 0%, rgba(124, 68, 68, 1) 100%); */
            /* background: linear-gradient(139deg, rgba(166, 203, 184, 1) 0%, rgba(131, 91, 91, 1) 100%); */
            background-image: url(./img/home.jpg);
            background-position: center;
            background-repeat: no-repeat;
            background-size: cover;
            z-index: 2;
        }
    </style>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
</HEAD>

<BODY>
    <?php
    session_start();
    include("header.php"); ?>
    <div class="overlay"></div>
    <div id="main" class="shadow-lg">
        <div id="vit_logo">
            <A HREF="index.php">
                <IMG SRC="img/white.png" alt="Home" id="vit_logo" width="150" height="150"></IMG>
            </A>
        </div>
        <h1 class="text-center mb-3 text-uppercase">Selamat Datang di</br>Politeknik Negeri Sambas</h1>
        <h4 class="text-center mb-4">Tentukan Tempat Parkirmu</h4>
        <?php
        if (isset($_SESSION['user_info'])) {
            echo '<h5 align="center"><a class="btn btn-sm btn-danger text-uppercase col-3" href="logout.php">Logout</a></h3>';
        } else {
            echo '<h6 align="center"><a href="register.php" class="btn btn-sm btn-success text-uppercase fw-light text-light text-decoration-none">Daftar/Login terlebih dahulu</a></h6>';
            echo '<h6 align="center"><a href="manager/manager.php" class="btn btn-sm btn-light text-uppercase fw-light text-dark text-decoration-none">Login Admin</a></h6>';
        }
        ?>
    </div>
</BODY>

</HTML>